$PBExportHeader$u_picture.sru
forward
global type u_picture from picturebutton
end type
end forward

global type u_picture from picturebutton
integer width = 119
integer height = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string picturename = "C:\TCCGabi\imagens\702988.png"
alignment htextalign = left!
end type
global u_picture u_picture

on u_picture.create
end on

on u_picture.destroy
end on

