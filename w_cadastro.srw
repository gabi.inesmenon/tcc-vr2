$PBExportHeader$w_cadastro.srw
forward
global type w_cadastro from w_padrao
end type
type sle_busca from singlelineedit within w_cadastro
end type
type pb_busca from u_picture within w_cadastro
end type
type st_busca from statictext within w_cadastro
end type
type cb_inseri from commandbutton within w_cadastro
end type
type cb_1 from commandbutton within w_cadastro
end type
type gb_busca from groupbox within w_cadastro
end type
end forward

global type w_cadastro from w_padrao
integer width = 3497
sle_busca sle_busca
pb_busca pb_busca
st_busca st_busca
cb_inseri cb_inseri
cb_1 cb_1
gb_busca gb_busca
end type
global w_cadastro w_cadastro

on w_cadastro.create
int iCurrent
call super::create
this.sle_busca=create sle_busca
this.pb_busca=create pb_busca
this.st_busca=create st_busca
this.cb_inseri=create cb_inseri
this.cb_1=create cb_1
this.gb_busca=create gb_busca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_busca
this.Control[iCurrent+2]=this.pb_busca
this.Control[iCurrent+3]=this.st_busca
this.Control[iCurrent+4]=this.cb_inseri
this.Control[iCurrent+5]=this.cb_1
this.Control[iCurrent+6]=this.gb_busca
end on

on w_cadastro.destroy
call super::destroy
destroy(this.sle_busca)
destroy(this.pb_busca)
destroy(this.st_busca)
destroy(this.cb_inseri)
destroy(this.cb_1)
destroy(this.gb_busca)
end on

type cb_ok from w_padrao`cb_ok within w_cadastro
integer weight = 400
end type

type cb_cancelar from w_padrao`cb_cancelar within w_cadastro
integer weight = 400
end type

type sle_busca from singlelineedit within w_cadastro
integer x = 37
integer y = 68
integer width = 402
integer height = 92
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type pb_busca from u_picture within w_cadastro
integer x = 457
integer y = 68
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
string facename = "Arial"
end type

type st_busca from statictext within w_cadastro
integer x = 599
integer y = 80
integer width = 2231
integer height = 72
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type cb_inseri from commandbutton within w_cadastro
integer x = 3218
integer y = 64
integer width = 215
integer height = 100
integer taborder = 30
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "+"
end type

type cb_1 from commandbutton within w_cadastro
integer x = 2843
integer y = 64
integer width = 343
integer height = 100
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ok"
end type

type gb_busca from groupbox within w_cadastro
integer x = 9
integer width = 3442
integer height = 212
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Busca "
end type

