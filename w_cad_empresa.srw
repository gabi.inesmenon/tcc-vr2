$PBExportHeader$w_cad_empresa.srw
forward
global type w_cad_empresa from w_cadastro
end type
type dw_emp from datawindow within w_cad_empresa
end type
type gb_1 from groupbox within w_cad_empresa
end type
end forward

global type w_cad_empresa from w_cadastro
integer width = 3479
integer height = 1168
string title = ""
dw_emp dw_emp
gb_1 gb_1
end type
global w_cad_empresa w_cad_empresa

on w_cad_empresa.create
int iCurrent
call super::create
this.dw_emp=create dw_emp
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_emp
this.Control[iCurrent+2]=this.gb_1
end on

on w_cad_empresa.destroy
call super::destroy
destroy(this.dw_emp)
destroy(this.gb_1)
end on

event open;call super::open;dw_emp.SetTransObject(SQLCA)

end event

type cb_ok from w_cadastro`cb_ok within w_cad_empresa
integer x = 2519
integer y = 932
end type

event cb_ok::clicked;call super::clicked;datawindow ldw_Gravar[]
ldw_Gravar[1] = dw_emp

f_update(ldw_Gravar)
end event

type cb_cancelar from w_cadastro`cb_cancelar within w_cad_empresa
integer x = 3026
integer y = 932
end type

type sle_busca from w_cadastro`sle_busca within w_cad_empresa
end type

type pb_busca from w_cadastro`pb_busca within w_cad_empresa
integer y = 64
end type

event pb_busca::clicked;call super::clicked;Long ll_Empresa
String ls_Nome

ll_Empresa = Long(sle_busca.Text)

SELECT
	IDEMPRESA,
	NOME
INTO
	:ll_Empresa,
	:ls_Nome
FROM
	public.empresa
WHERE
	IDEMPRESA = :ll_Empresa
USING SQLCA;

If ll_Empresa = 0 Then
	MessageBox('Cadastro', 'Nenhuma empresa encontrada!')
Else	
	st_busca.Text = ls_Nome
End if
end event

type st_busca from w_cadastro`st_busca within w_cad_empresa
integer x = 594
integer y = 84
integer width = 2217
end type

type cb_inseri from w_cadastro`cb_inseri within w_cad_empresa
integer x = 3205
integer height = 104
end type

event cb_inseri::clicked;call super::clicked;dw_emp.Reset()
dw_emp.InsertRow(0)
end event

type cb_1 from w_cadastro`cb_1 within w_cad_empresa
integer x = 2830
end type

event cb_1::clicked;call super::clicked;dw_emp.Retrieve(Long(sle_busca.Text))
end event

type gb_busca from w_cadastro`gb_busca within w_cad_empresa
integer width = 3419
end type

type dw_emp from datawindow within w_cad_empresa
integer x = 78
integer y = 312
integer width = 3323
integer height = 544
integer taborder = 10
string title = "none"
string dataobject = "d_cad_empresa"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type gb_1 from groupbox within w_cad_empresa
integer y = 212
integer width = 3433
integer height = 676
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Cadastro de Empresa "
end type

