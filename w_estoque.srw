$PBExportHeader$w_estoque.srw
forward
global type w_estoque from w_padrao
end type
type dw_produtos from datawindow within w_estoque
end type
type sle_buscaprod from singlelineedit within w_estoque
end type
type uo_1 from u_picture within w_estoque
end type
type st_descrprod from statictext within w_estoque
end type
type gb_2 from groupbox within w_estoque
end type
type gb_1 from groupbox within w_estoque
end type
end forward

global type w_estoque from w_padrao
integer width = 2962
integer height = 1760
dw_produtos dw_produtos
sle_buscaprod sle_buscaprod
uo_1 uo_1
st_descrprod st_descrprod
gb_2 gb_2
gb_1 gb_1
end type
global w_estoque w_estoque

on w_estoque.create
int iCurrent
call super::create
this.dw_produtos=create dw_produtos
this.sle_buscaprod=create sle_buscaprod
this.uo_1=create uo_1
this.st_descrprod=create st_descrprod
this.gb_2=create gb_2
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_produtos
this.Control[iCurrent+2]=this.sle_buscaprod
this.Control[iCurrent+3]=this.uo_1
this.Control[iCurrent+4]=this.st_descrprod
this.Control[iCurrent+5]=this.gb_2
this.Control[iCurrent+6]=this.gb_1
end on

on w_estoque.destroy
call super::destroy
destroy(this.dw_produtos)
destroy(this.sle_buscaprod)
destroy(this.uo_1)
destroy(this.st_descrprod)
destroy(this.gb_2)
destroy(this.gb_1)
end on

event open;call super::open;dw_produtos.SetTransObject(SQLCA)

end event

type cb_ok from w_padrao`cb_ok within w_estoque
integer x = 2459
integer y = 72
string text = "Pesquisar"
end type

event cb_ok::clicked;call super::clicked;dw_produtos.Retrieve(Long(sle_buscaprod.Text))
end event

type cb_cancelar from w_padrao`cb_cancelar within w_estoque
boolean visible = false
integer x = 3808
integer y = 1680
end type

type dw_produtos from datawindow within w_estoque
integer x = 23
integer y = 292
integer width = 2862
integer height = 1304
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_estoque_saldo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type sle_buscaprod from singlelineedit within w_estoque
integer x = 46
integer y = 80
integer width = 361
integer height = 88
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type uo_1 from u_picture within w_estoque
integer x = 421
integer y = 68
integer width = 114
integer height = 104
integer taborder = 30
boolean bringtotop = true
end type

on uo_1.destroy
call u_picture::destroy
end on

event clicked;call super::clicked;Long ll_Produto, ll_Row
String ls_Descr
Date ldt_Validade

ll_Produto = Long(sle_buscaprod.Text)

SELECT
	IDPRODUTO,
	DESCRICAO,
	DTVALIDADE
INTO
	:ll_Produto,
	:ls_Descr,
	:ldt_Validade
FROM
	public.produto
WHERE
	IDPRODUTO = :ll_Produto
USING SQLCA;

if ldt_Validade < Today() Then
	MessageBox('Vendas','Produto com data de validade vencida, verifique!')
	Return
End if

st_descrprod.Text = ls_Descr
end event

type st_descrprod from statictext within w_estoque
integer x = 544
integer y = 84
integer width = 1838
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type gb_2 from groupbox within w_estoque
integer width = 2912
integer height = 204
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Produtos "
end type

type gb_1 from groupbox within w_estoque
integer y = 216
integer width = 2912
integer height = 1420
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Movimentações"
end type

