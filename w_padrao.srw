$PBExportHeader$w_padrao.srw
forward
global type w_padrao from window
end type
type cb_ok from commandbutton within w_padrao
end type
type cb_cancelar from commandbutton within w_padrao
end type
end forward

global type w_padrao from window
integer width = 3378
integer height = 1408
boolean titlebar = true
string title = "TCC Gabriela "
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_ok cb_ok
cb_cancelar cb_cancelar
end type
global w_padrao w_padrao

on w_padrao.create
this.cb_ok=create cb_ok
this.cb_cancelar=create cb_cancelar
this.Control[]={this.cb_ok,&
this.cb_cancelar}
end on

on w_padrao.destroy
destroy(this.cb_ok)
destroy(this.cb_cancelar)
end on

event open;SQLCA.DBParm = "ConnectString='DSN=postgreSQL35W;UID=postgres;PWD=postgres'"
end event

type cb_ok from commandbutton within w_padrao
integer x = 2130
integer y = 1120
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ok"
end type

type cb_cancelar from commandbutton within w_padrao
integer x = 2610
integer y = 1120
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cancelar"
end type

event clicked;Close(Parent)
end event

