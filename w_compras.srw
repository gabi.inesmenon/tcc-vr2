$PBExportHeader$w_compras.srw
forward
global type w_compras from w_padrao
end type
type uo_picturebusca from u_picture within w_compras
end type
type sle_buscaemp from singlelineedit within w_compras
end type
type st_emp from statictext within w_compras
end type
type cb_1 from commandbutton within w_compras
end type
type cb_2 from commandbutton within w_compras
end type
type dw_produtos from datawindow within w_compras
end type
type sle_buscaprod from singlelineedit within w_compras
end type
type uo_1 from u_picture within w_compras
end type
type st_descrprod from statictext within w_compras
end type
type gb_1 from groupbox within w_compras
end type
type gb_2 from groupbox within w_compras
end type
end forward

global type w_compras from w_padrao
integer width = 4608
integer height = 2064
uo_picturebusca uo_picturebusca
sle_buscaemp sle_buscaemp
st_emp st_emp
cb_1 cb_1
cb_2 cb_2
dw_produtos dw_produtos
sle_buscaprod sle_buscaprod
uo_1 uo_1
st_descrprod st_descrprod
gb_1 gb_1
gb_2 gb_2
end type
global w_compras w_compras

type variables
Datastore Ids_Saldo
n_estoque In_estoque
end variables
forward prototypes
public subroutine of_gravar ()
end prototypes

public subroutine of_gravar ();Long ll_For
datawindow ldw_Gravar[]
ldw_Gravar[1] = dw_produtos

f_update(ldw_Gravar)

For ll_For = 1 To dw_produtos.RowCount()
	In_estoque.of_insere_estoque(dw_produtos.GetItemNumber(ll_For, 'idempresa'), dw_produtos.GetItemNumber(ll_For, 'idproduto'), dw_produtos.GetItemNumber(ll_For, 'qtdproduto'))
Next
end subroutine

on w_compras.create
int iCurrent
call super::create
this.uo_picturebusca=create uo_picturebusca
this.sle_buscaemp=create sle_buscaemp
this.st_emp=create st_emp
this.cb_1=create cb_1
this.cb_2=create cb_2
this.dw_produtos=create dw_produtos
this.sle_buscaprod=create sle_buscaprod
this.uo_1=create uo_1
this.st_descrprod=create st_descrprod
this.gb_1=create gb_1
this.gb_2=create gb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_picturebusca
this.Control[iCurrent+2]=this.sle_buscaemp
this.Control[iCurrent+3]=this.st_emp
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.cb_2
this.Control[iCurrent+6]=this.dw_produtos
this.Control[iCurrent+7]=this.sle_buscaprod
this.Control[iCurrent+8]=this.uo_1
this.Control[iCurrent+9]=this.st_descrprod
this.Control[iCurrent+10]=this.gb_1
this.Control[iCurrent+11]=this.gb_2
end on

on w_compras.destroy
call super::destroy
destroy(this.uo_picturebusca)
destroy(this.sle_buscaemp)
destroy(this.st_emp)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.dw_produtos)
destroy(this.sle_buscaprod)
destroy(this.uo_1)
destroy(this.st_descrprod)
destroy(this.gb_1)
destroy(this.gb_2)
end on

event open;call super::open;dw_produtos.SetTransObject(SQLCA)
Ids_Saldo = Create Datastore
Ids_Saldo.SetTransObject(SQLCA)
In_estoque = Create n_estoque
end event

type cb_ok from w_padrao`cb_ok within w_compras
integer x = 3648
integer y = 1824
string text = "Gravar"
end type

event cb_ok::clicked;call super::clicked;of_gravar()
end event

type cb_cancelar from w_padrao`cb_cancelar within w_compras
boolean visible = false
integer x = 2921
integer y = 1216
end type

type uo_picturebusca from u_picture within w_compras
integer x = 416
integer y = 76
integer width = 114
integer height = 104
integer taborder = 20
boolean bringtotop = true
end type

on uo_picturebusca.destroy
call u_picture::destroy
end on

event clicked;call super::clicked;Long ll_Emp
String ls_Nome

ll_Emp = Long(sle_buscaemp.Text)

SELECT
	IDEMPRESA,
	NOME
INTO
	:ll_Emp,
	:ls_Nome
FROM
	public.empresa
WHERE
	IDEMPRESA = :ll_Emp
USING SQLCA;

If ll_Emp = 0 Then
	MessageBox('Vendas', 'Nenhum cliente encontrado!')
Else	
	st_emp.Text = ls_Nome
End if
end event

type sle_buscaemp from singlelineedit within w_compras
integer x = 46
integer y = 84
integer width = 361
integer height = 88
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_emp from statictext within w_compras
integer x = 544
integer y = 100
integer width = 3963
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_compras
integer x = 4338
integer y = 316
integer width = 178
integer height = 112
integer taborder = 20
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "+"
end type

event clicked;Long ll_Row


sle_buscaemp.Enabled = False

ll_Row = dw_produtos.InsertRow(0)
dw_produtos.SetItem(ll_Row, 'idproduto', Long(sle_buscaprod.text))
dw_produtos.SetItem(ll_Row, 'descricao', st_descrprod.Text)
dw_produtos.SetItem(ll_Row, 'idempresa', Long(sle_buscaemp.Text))
dw_produtos.SetItem(ll_Row, 'dtcompra', today())

end event

type cb_2 from commandbutton within w_compras
integer x = 4133
integer y = 1824
integer width = 402
integer height = 112
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cancelar"
end type

type dw_produtos from datawindow within w_compras
integer x = 27
integer y = 456
integer width = 4475
integer height = 1304
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_compra_prod"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type sle_buscaprod from singlelineedit within w_compras
integer x = 46
integer y = 316
integer width = 361
integer height = 88
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type uo_1 from u_picture within w_compras
integer x = 411
integer y = 312
integer width = 114
integer height = 104
integer taborder = 30
boolean bringtotop = true
end type

on uo_1.destroy
call u_picture::destroy
end on

event clicked;call super::clicked;Long ll_Produto, ll_Row
String ls_Descr
Date ldt_Validade

ll_Produto = Long(sle_buscaprod.Text)

SELECT
	IDPRODUTO,
	DESCRICAO,
	DTVALIDADE
INTO
	:ll_Produto,
	:ls_Descr,
	:ldt_Validade
FROM
	public.produto
WHERE
	IDPRODUTO = :ll_Produto
USING SQLCA;

If ll_Produto = 0 Then
	MessageBox('Vendas', 'Nenhum cliente encontrado!')
	Return
Else	
	st_descrprod.Text = ls_Descr
End if

if ldt_Validade < Today() Then
	MessageBox('Vendas','Produto com data de validade vencida, verifique!')
	Return
End if



end event

type st_descrprod from statictext within w_compras
integer x = 544
integer y = 328
integer width = 3753
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_compras
integer width = 4544
integer height = 212
integer taborder = 20
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Empresa"
end type

type gb_2 from groupbox within w_compras
integer y = 224
integer width = 4544
integer height = 1572
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Produtos "
end type

