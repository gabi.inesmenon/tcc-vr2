$PBExportHeader$w_vendas.srw
forward
global type w_vendas from w_padrao
end type
type tab_venda from tab within w_vendas
end type
type tabpage_lanca from userobject within tab_venda
end type
type cb_2 from commandbutton within tabpage_lanca
end type
type cb_1 from commandbutton within tabpage_lanca
end type
type dw_produtos from datawindow within tabpage_lanca
end type
type cb_3 from commandbutton within tabpage_lanca
end type
type st_descrprod from statictext within tabpage_lanca
end type
type pb_2 from u_picture within tabpage_lanca
end type
type sle_buscaprod from singlelineedit within tabpage_lanca
end type
type ddlb_emp from dropdownpicturelistbox within tabpage_lanca
end type
type st_3 from statictext within tabpage_lanca
end type
type st_cli from statictext within tabpage_lanca
end type
type uo_picturebusca from u_picture within tabpage_lanca
end type
type sle_buscacli from singlelineedit within tabpage_lanca
end type
type gb_1 from groupbox within tabpage_lanca
end type
type gb_2 from groupbox within tabpage_lanca
end type
type tabpage_lanca from userobject within tab_venda
cb_2 cb_2
cb_1 cb_1
dw_produtos dw_produtos
cb_3 cb_3
st_descrprod st_descrprod
pb_2 pb_2
sle_buscaprod sle_buscaprod
ddlb_emp ddlb_emp
st_3 st_3
st_cli st_cli
uo_picturebusca uo_picturebusca
sle_buscacli sle_buscacli
gb_1 gb_1
gb_2 gb_2
end type
type tabpage_pesquisa from userobject within tab_venda
end type
type cb_pesquisa from commandbutton within tabpage_pesquisa
end type
type st_produto from statictext within tabpage_pesquisa
end type
type st_cliente from statictext within tabpage_pesquisa
end type
type st_emp from statictext within tabpage_pesquisa
end type
type dw_pesquisa from datawindow within tabpage_pesquisa
end type
type pb_4 from u_picture within tabpage_pesquisa
end type
type pb_3 from u_picture within tabpage_pesquisa
end type
type pb_1 from u_picture within tabpage_pesquisa
end type
type st_4 from statictext within tabpage_pesquisa
end type
type st_2 from statictext within tabpage_pesquisa
end type
type st_1 from statictext within tabpage_pesquisa
end type
type sle_prod from singlelineedit within tabpage_pesquisa
end type
type sle_cli from singlelineedit within tabpage_pesquisa
end type
type sle_emp from singlelineedit within tabpage_pesquisa
end type
type dw_data from datawindow within tabpage_pesquisa
end type
type gb_3 from groupbox within tabpage_pesquisa
end type
type gb_4 from groupbox within tabpage_pesquisa
end type
type tabpage_pesquisa from userobject within tab_venda
cb_pesquisa cb_pesquisa
st_produto st_produto
st_cliente st_cliente
st_emp st_emp
dw_pesquisa dw_pesquisa
pb_4 pb_4
pb_3 pb_3
pb_1 pb_1
st_4 st_4
st_2 st_2
st_1 st_1
sle_prod sle_prod
sle_cli sle_cli
sle_emp sle_emp
dw_data dw_data
gb_3 gb_3
gb_4 gb_4
end type
type tab_venda from tab within w_vendas
tabpage_lanca tabpage_lanca
tabpage_pesquisa tabpage_pesquisa
end type
end forward

global type w_vendas from w_padrao
integer width = 4649
integer height = 2184
string title = "Vendas"
string icon = "Window!"
tab_venda tab_venda
end type
global w_vendas w_vendas

type variables
n_estoque In_estoque
end variables

forward prototypes
public subroutine of_gravar ()
public subroutine of_pesquisa ()
end prototypes

public subroutine of_gravar ();Long ll_For
datawindow ldw_Gravar[]
ldw_Gravar[1] = tab_venda.tabpage_lanca.dw_produtos

f_update(ldw_Gravar)

For ll_For = 1 To tab_venda.tabpage_lanca.dw_produtos.RowCount()
	In_estoque.of_atualiza_estoque(tab_venda.tabpage_lanca.dw_produtos.GetItemNumber(ll_For, 'vendaprod_idempresa'), &
	tab_venda.tabpage_lanca.dw_produtos.GetItemNumber(ll_For, 'idproduto'), tab_venda.tabpage_lanca.dw_produtos.GetItemNumber(ll_For, 'vendaprod_qtdproduto'))
Next
end subroutine

public subroutine of_pesquisa ();Long ll_Emp, ll_Cli, ll_Prod
tab_venda.tabpage_pesquisa.dw_data.AcceptText()
if Long(tab_venda.tabpage_pesquisa.sle_emp.Text) > 0 Then
	ll_Emp = Long(tab_venda.tabpage_pesquisa.sle_emp.Text)
End if

if Long(tab_venda.tabpage_pesquisa.sle_cli.Text) > 0 Then
	ll_Cli = Long(tab_venda.tabpage_pesquisa.sle_cli.Text)
End if

if Long(tab_venda.tabpage_pesquisa.sle_prod.Text) > 0 Then
	ll_Prod = Long(tab_venda.tabpage_pesquisa.sle_prod.Text)
End if

if tab_venda.tabpage_pesquisa.dw_data.GetItemDate(1, 'datainicio') <= Date('01/01/1900') Or Isnull( tab_venda.tabpage_pesquisa.dw_data.GetItemDate(1, 'datainicio')) Or &
	tab_venda.tabpage_pesquisa.dw_data.GetItemDate(1, 'datafim') <= Date('01/01/1900') Or Isnull( tab_venda.tabpage_pesquisa.dw_data.GetItemDate(1, 'datafim')) Then
	MessageBox('Vendas', 'Data não selecionada.')
	Return
End if

tab_venda.tabpage_pesquisa.dw_pesquisa.Retrieve(ll_Emp, ll_Cli, ll_Prod, tab_venda.tabpage_pesquisa.dw_data.GetItemDate(1, 'dataincio'), tab_venda.tabpage_pesquisa.dw_data.GetItemDate(1, 'datafim'))

end subroutine

on w_vendas.create
int iCurrent
call super::create
this.tab_venda=create tab_venda
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_venda
end on

on w_vendas.destroy
call super::destroy
destroy(this.tab_venda)
end on

event open;call super::open;tab_venda.tabpage_lanca.dw_produtos.SetTransObject(SQLCA)
tab_venda.tabpage_pesquisa.dw_data.SetTransObject(SQLCA)
tab_venda.tabpage_pesquisa.dw_pesquisa.SetTransObject(SQLCA)
tab_venda.tabpage_pesquisa.dw_data.Insertrow(0)

In_estoque = create n_estoque
end event

type cb_ok from w_padrao`cb_ok within w_vendas
boolean visible = false
integer x = 5189
integer y = 1572
string text = "Gravar"
end type

type cb_cancelar from w_padrao`cb_cancelar within w_vendas
boolean visible = false
integer x = 5207
integer y = 1316
end type

type tab_venda from tab within w_vendas
integer width = 4599
integer height = 2056
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
boolean raggedright = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
tabpage_lanca tabpage_lanca
tabpage_pesquisa tabpage_pesquisa
end type

on tab_venda.create
this.tabpage_lanca=create tabpage_lanca
this.tabpage_pesquisa=create tabpage_pesquisa
this.Control[]={this.tabpage_lanca,&
this.tabpage_pesquisa}
end on

on tab_venda.destroy
destroy(this.tabpage_lanca)
destroy(this.tabpage_pesquisa)
end on

type tabpage_lanca from userobject within tab_venda
event create ( )
event destroy ( )
integer x = 18
integer y = 104
integer width = 4562
integer height = 1936
long backcolor = 67108864
string text = "Lançamento"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
cb_2 cb_2
cb_1 cb_1
dw_produtos dw_produtos
cb_3 cb_3
st_descrprod st_descrprod
pb_2 pb_2
sle_buscaprod sle_buscaprod
ddlb_emp ddlb_emp
st_3 st_3
st_cli st_cli
uo_picturebusca uo_picturebusca
sle_buscacli sle_buscacli
gb_1 gb_1
gb_2 gb_2
end type

on tabpage_lanca.create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_produtos=create dw_produtos
this.cb_3=create cb_3
this.st_descrprod=create st_descrprod
this.pb_2=create pb_2
this.sle_buscaprod=create sle_buscaprod
this.ddlb_emp=create ddlb_emp
this.st_3=create st_3
this.st_cli=create st_cli
this.uo_picturebusca=create uo_picturebusca
this.sle_buscacli=create sle_buscacli
this.gb_1=create gb_1
this.gb_2=create gb_2
this.Control[]={this.cb_2,&
this.cb_1,&
this.dw_produtos,&
this.cb_3,&
this.st_descrprod,&
this.pb_2,&
this.sle_buscaprod,&
this.ddlb_emp,&
this.st_3,&
this.st_cli,&
this.uo_picturebusca,&
this.sle_buscacli,&
this.gb_1,&
this.gb_2}
end on

on tabpage_lanca.destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_produtos)
destroy(this.cb_3)
destroy(this.st_descrprod)
destroy(this.pb_2)
destroy(this.sle_buscaprod)
destroy(this.ddlb_emp)
destroy(this.st_3)
destroy(this.st_cli)
destroy(this.uo_picturebusca)
destroy(this.sle_buscacli)
destroy(this.gb_1)
destroy(this.gb_2)
end on

type cb_2 from commandbutton within tabpage_lanca
integer x = 3694
integer y = 1804
integer width = 402
integer height = 112
integer taborder = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gravar"
end type

event clicked;of_gravar()
end event

type cb_1 from commandbutton within tabpage_lanca
integer x = 4142
integer y = 1804
integer width = 402
integer height = 112
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cancelar"
end type

type dw_produtos from datawindow within tabpage_lanca
integer x = 27
integer y = 456
integer width = 4475
integer height = 1304
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_venda_prod"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_3 from commandbutton within tabpage_lanca
integer x = 4338
integer y = 316
integer width = 178
integer height = 112
integer taborder = 30
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "+"
end type

event clicked;Long ll_Row

sle_buscacli.Enabled = False
ddlb_emp.Enabled = False
 
ll_Row = dw_produtos.InsertRow(0)
dw_produtos.SetItem(ll_Row, 'idproduto', Long(sle_buscaprod.text))
dw_produtos.SetItem(ll_Row, 'produto_descricao', st_descrprod.Text)
dw_produtos.SetItem(ll_Row, 'vendaprod_idempresa', Long(ddlb_emp.Text))
dw_produtos.SetItem(ll_Row, 'idcliente', Long(sle_buscacli.Text))
dw_produtos.SetItem(ll_Row, 'dtvenda', today())
end event

type st_descrprod from statictext within tabpage_lanca
integer x = 553
integer y = 336
integer width = 3753
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type pb_2 from u_picture within tabpage_lanca
integer x = 411
integer y = 308
integer width = 114
integer height = 104
integer taborder = 40
boolean bringtotop = true
end type

event clicked;call super::clicked;Long ll_Produto, ll_Row
String ls_Descr
Date ldt_Validade

ll_Produto = Long(sle_buscaprod.Text)
//
//SELECT
//	IDPRODUTO,
//	DESCRICAO,
//	DTVALIDADE
//INTO
//	:ll_Produto,
//	:ls_Descr,
//	:ldt_Validade
//FROM
//	public.produto
//WHERE
//	IDPRODUTO = :ll_Produto
//USING SQLCA;

If ll_Produto = 0 Then
	MessageBox('Vendas', 'Nenhum cliente encontrado!')
	Return
Else	
	st_descrprod.Text = ls_Descr
End if

if ldt_Validade < Today() Then
	MessageBox('Vendas','Produto com data de validade vencida, verifique!')
	Return
End if



end event

type sle_buscaprod from singlelineedit within tabpage_lanca
integer x = 46
integer y = 316
integer width = 361
integer height = 88
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type ddlb_emp from dropdownpicturelistbox within tabpage_lanca
integer x = 4274
integer y = 92
integer width = 238
integer height = 352
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string item[] = {"1","2","3"}
borderstyle borderstyle = stylelowered!
integer itempictureindex[] = {1,2,3}
long picturemaskcolor = 536870912
end type

type st_3 from statictext within tabpage_lanca
integer x = 3913
integer y = 96
integer width = 343
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Empresa"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_cli from statictext within tabpage_lanca
integer x = 544
integer y = 88
integer width = 3310
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type uo_picturebusca from u_picture within tabpage_lanca
integer x = 416
integer y = 72
integer width = 114
integer height = 104
integer taborder = 30
boolean bringtotop = true
end type

event clicked;call super::clicked;Long ll_Cliente
String ls_Nome

ll_Cliente = Long(sle_buscacli.Text)

//SELECT
//	IDCLIENTE,
//	NOME
//INTO
//	:ll_Cliente,
//	:ls_Nome
//FROM
//	public.cliente
//WHERE
//	IDCLIENTE = :ll_Cliente
//USING SQLCA;
//
If ll_Cliente = 0 Then
	MessageBox('Vendas', 'Nenhum cliente encontrado!')
Else	
	st_cli.Text = ls_Nome
End if
end event

type sle_buscacli from singlelineedit within tabpage_lanca
integer x = 46
integer y = 84
integer width = 361
integer height = 88
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type gb_1 from groupbox within tabpage_lanca
integer x = 9
integer y = 4
integer width = 4530
integer height = 208
integer taborder = 30
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Cliente "
end type

type gb_2 from groupbox within tabpage_lanca
integer x = 9
integer y = 232
integer width = 4530
integer height = 1552
integer taborder = 40
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Cliente "
end type

type tabpage_pesquisa from userobject within tab_venda
integer x = 18
integer y = 104
integer width = 4562
integer height = 1936
long backcolor = 67108864
string text = "Pesquisa"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
cb_pesquisa cb_pesquisa
st_produto st_produto
st_cliente st_cliente
st_emp st_emp
dw_pesquisa dw_pesquisa
pb_4 pb_4
pb_3 pb_3
pb_1 pb_1
st_4 st_4
st_2 st_2
st_1 st_1
sle_prod sle_prod
sle_cli sle_cli
sle_emp sle_emp
dw_data dw_data
gb_3 gb_3
gb_4 gb_4
end type

on tabpage_pesquisa.create
this.cb_pesquisa=create cb_pesquisa
this.st_produto=create st_produto
this.st_cliente=create st_cliente
this.st_emp=create st_emp
this.dw_pesquisa=create dw_pesquisa
this.pb_4=create pb_4
this.pb_3=create pb_3
this.pb_1=create pb_1
this.st_4=create st_4
this.st_2=create st_2
this.st_1=create st_1
this.sle_prod=create sle_prod
this.sle_cli=create sle_cli
this.sle_emp=create sle_emp
this.dw_data=create dw_data
this.gb_3=create gb_3
this.gb_4=create gb_4
this.Control[]={this.cb_pesquisa,&
this.st_produto,&
this.st_cliente,&
this.st_emp,&
this.dw_pesquisa,&
this.pb_4,&
this.pb_3,&
this.pb_1,&
this.st_4,&
this.st_2,&
this.st_1,&
this.sle_prod,&
this.sle_cli,&
this.sle_emp,&
this.dw_data,&
this.gb_3,&
this.gb_4}
end on

on tabpage_pesquisa.destroy
destroy(this.cb_pesquisa)
destroy(this.st_produto)
destroy(this.st_cliente)
destroy(this.st_emp)
destroy(this.dw_pesquisa)
destroy(this.pb_4)
destroy(this.pb_3)
destroy(this.pb_1)
destroy(this.st_4)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.sle_prod)
destroy(this.sle_cli)
destroy(this.sle_emp)
destroy(this.dw_data)
destroy(this.gb_3)
destroy(this.gb_4)
end on

type cb_pesquisa from commandbutton within tabpage_pesquisa
integer x = 4142
integer y = 272
integer width = 343
integer height = 100
integer taborder = 50
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pesquisar"
end type

event clicked;of_pesquisa()
end event

type st_produto from statictext within tabpage_pesquisa
integer x = 891
integer y = 292
integer width = 2601
integer height = 72
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type st_cliente from statictext within tabpage_pesquisa
integer x = 891
integer y = 188
integer width = 2601
integer height = 72
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type st_emp from statictext within tabpage_pesquisa
integer x = 891
integer y = 72
integer width = 2601
integer height = 72
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
boolean focusrectangle = false
end type

type dw_pesquisa from datawindow within tabpage_pesquisa
integer x = 46
integer y = 480
integer width = 4462
integer height = 1420
integer taborder = 40
boolean enabled = false
string title = "none"
string dataobject = "d_venda_prod"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type pb_4 from u_picture within tabpage_pesquisa
integer x = 763
integer y = 280
integer width = 114
integer height = 96
integer taborder = 80
end type

type pb_3 from u_picture within tabpage_pesquisa
integer x = 763
integer y = 176
integer width = 114
integer height = 96
integer taborder = 70
end type

type pb_1 from u_picture within tabpage_pesquisa
integer x = 763
integer y = 60
integer width = 114
integer height = 96
integer taborder = 60
end type

type st_4 from statictext within tabpage_pesquisa
integer x = 46
integer y = 288
integer width = 343
integer height = 56
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Produto"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within tabpage_pesquisa
integer x = 46
integer y = 180
integer width = 343
integer height = 56
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Cliente"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within tabpage_pesquisa
integer x = 46
integer y = 72
integer width = 343
integer height = 56
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Empresa"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_prod from singlelineedit within tabpage_pesquisa
integer x = 407
integer y = 284
integer width = 343
integer height = 76
integer taborder = 70
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_cli from singlelineedit within tabpage_pesquisa
integer x = 407
integer y = 176
integer width = 343
integer height = 76
integer taborder = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_emp from singlelineedit within tabpage_pesquisa
integer x = 407
integer y = 68
integer width = 343
integer height = 76
integer taborder = 50
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type dw_data from datawindow within tabpage_pesquisa
integer x = 3525
integer y = 44
integer width = 978
integer height = 140
integer taborder = 60
string title = "none"
string dataobject = "d_periodo"
boolean border = false
boolean livescroll = true
end type

type gb_3 from groupbox within tabpage_pesquisa
integer x = 18
integer y = 8
integer width = 4530
integer height = 392
integer taborder = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Filtros "
end type

type gb_4 from groupbox within tabpage_pesquisa
integer x = 18
integer y = 416
integer width = 4530
integer height = 1500
integer taborder = 70
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Filtros "
end type

