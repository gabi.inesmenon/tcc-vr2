$PBExportHeader$teste.sra
$PBExportComments$Generated Application Object
forward
global type teste from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
transaction SQLCA
end variables
global type teste from application
string appname = "teste"
end type
global teste teste

on teste.create
appname="teste"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on teste.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;//SQLCA = Create Transaction

//// Define as informações de conexão do banco de dados
//SQLCA.DBMS = "ODBC"
//SQLCA.Database = "postgres"
//SQLCA.ServerName = "servidor"
//SQLCA.LogId = "postgres"
//SQLCA.LogPass = "postgres"
////SQLCA.DBParm="ConnectString='DSN=Sample'"
//
////SQLCA.SetTransObject(SQLCA)
//
//SQLCA.DBParm = "ConnectString='DSN=Sample'DBMS=ODBC;Database=postgres;ServerName=localhost;UserID=postgres;Password=postgres"
//
//
SQLCA.DBMS = "ODBC"
SQLCA.AutoCommit = False
SQLCA.DBParm = "ConnectString='DSN=PostgreSQL35W;UID=postgres;PWD=postgres'"
// Connect to the Adaptive Server Anywhere database.
CONNECT USING SQLCA;
// Declare the ASE Transaction object.
//transaction ASETrans
//// Create the ASE Transaction object.
//ASETrans = CREATE TRANSACTION
//// Set the ASE Transaction object properties.
//ASETrans.DBMS = "ODBC"
//ASETrans.Database = "postgres"
//ASETrans.LogID = "postgres"
//ASETrans.LogPass = "postgres"
//ASETrans.ServerName = "servidor"
//
//CONNECT USING ASETrans;

INSERT INTO PRODUTO VALUES (  3, 'BOSTON' ) USING SQLCA;

COMMIT;

//CONNECT USING SQLCA;
open(w_iniciar)
end event

