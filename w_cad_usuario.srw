$PBExportHeader$w_cad_usuario.srw
forward
global type w_cad_usuario from w_cadastro
end type
type dw_user from datawindow within w_cad_usuario
end type
type gb_1 from groupbox within w_cad_usuario
end type
end forward

global type w_cad_usuario from w_cadastro
integer width = 2679
integer height = 896
string title = "Cadastro de usuários"
dw_user dw_user
gb_1 gb_1
end type
global w_cad_usuario w_cad_usuario

on w_cad_usuario.create
int iCurrent
call super::create
this.dw_user=create dw_user
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_user
this.Control[iCurrent+2]=this.gb_1
end on

on w_cad_usuario.destroy
call super::destroy
destroy(this.dw_user)
destroy(this.gb_1)
end on

event open;dw_user.SetTransObject(SQLCA)

end event

type cb_ok from w_cadastro`cb_ok within w_cad_usuario
integer x = 1765
integer y = 660
end type

event cb_ok::clicked;call super::clicked;datawindow ldw_Gravar[]
ldw_Gravar[1] = dw_user

f_update(ldw_Gravar)
end event

type cb_cancelar from w_cadastro`cb_cancelar within w_cad_usuario
integer x = 2222
integer y = 660
end type

type sle_busca from w_cadastro`sle_busca within w_cad_usuario
end type

type pb_busca from w_cadastro`pb_busca within w_cad_usuario
end type

event pb_busca::clicked;call super::clicked;Long ll_User
String ls_Nome

ll_User = Long(sle_busca.Text)

SELECT
	IDUSUARIO,
	NOME
INTO
	:ll_User,
	:ls_Nome
FROM
	public.usuarios
WHERE
	IDUSUARIO = :ll_User
USING SQLCA;

If ll_User = 0 Then
	MessageBox('Cadastro', 'Nenhum usuário encontrado!')
Else	
	st_busca.Text = ls_Nome
End if
end event

type st_busca from w_cadastro`st_busca within w_cad_usuario
integer width = 1413
end type

type cb_inseri from w_cadastro`cb_inseri within w_cad_usuario
integer x = 2409
end type

event cb_inseri::clicked;call super::clicked;dw_user.Reset()
dw_user.InsertRow(0) 
end event

type cb_1 from w_cadastro`cb_1 within w_cad_usuario
integer x = 2034
end type

event cb_1::clicked;call super::clicked;dw_user.Retrieve(Long(sle_busca.Text))
end event

type gb_busca from w_cadastro`gb_busca within w_cad_usuario
integer width = 2624
end type

type dw_user from datawindow within w_cad_usuario
integer x = 46
integer y = 288
integer width = 2574
integer height = 316
integer taborder = 20
string title = "none"
string dataobject = "d_cad_usuario"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type gb_1 from groupbox within w_cad_usuario
integer x = 18
integer y = 216
integer width = 2606
integer height = 408
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Usuário"
end type

